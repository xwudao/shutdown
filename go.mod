module gitee.com/xwudao/shutdown

go 1.15

require (
	gitee.com/xwudao/wlog v0.0.6
	github.com/lestrrat-go/strftime v1.0.4 // indirect
	golang.org/x/sys v0.0.0-20210113181707-4bcb84eeeb78 // indirect
)
